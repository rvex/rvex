
.PHONY: all
all: download-platforms legacy/rvex-rewrite/tools/vex-3.43

.PHONY: download-platforms
download-platforms:
	$(MAKE) -C platforms

legacy/rvex-rewrite/tools/vex-3.43:
	$(MAKE) -C legacy
