Backward-compatible toolchain builder
=====================================

This directory contains scripts to build a toolchain that the old platforms etc.
(internally known as the rvex-rewrite repository) can work with.

Note that the `config` folder of rvex-rewrite is defunct when using this system.
To change the r-VEX configuration, change the `config-*.ini` files here and
rebuild instead. This is much more convenient than the old system anyway,
because the toolchain should remain in sync (better) with the new system.


Usage
-----

Using release 4.1 or 4.2:

 - Unpack the release here and rename the `rvex-release-4.x` folder to
   `rvex-rewrite`.
 - Run `make` and grab a coffee.

Using the (TU Delft internal) rvex-rewrite repository:

 - Clone rvex-rewrite here.
 - Run `make` and grab a coffee.

You can also run `make` first and then copy the generated `rvex-rewrite` folder
into your release 4.x-like tree.


Troubleshooting
---------------

The scripts in this directory simply call those in `../tools` with appropriate
configurations, so if you run into problems here, first try to get the scripts
in that directory working (follow the readme file in that directory).
