Platform directory
==================

This directory is where platform git repositories should be cloned. You can find
these on https://bitbucket.org/rvex and searching for `plat-`. At the time of
writing, that would be https://bitbucket.org/rvex/profile/repositories?search=plat-
