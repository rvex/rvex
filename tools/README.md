Toolchain builder
=================

This set of scripts should help you build the r-VEX VHDL sources, documentation,
and a complete toolchain for compiling C programs, given only a single
configuration file. The scripts will automagically download everything you need
(save for dependencies that you should get from your OS repositories;
unfortunately you'll have to figure those out yourself, but you should get quite
far with `build-essential` and `python3`) and try to properly manage incremental
builds for you.


Basic usage
-----------

 - Make sure you have at least about 2 GB of free space.
 - Make a configuration directory somewhere. Doesn't matter where.
 - Copy `tools/example-config.ini` to your configuration directory and rename it
   to `config.ini`.
 - Modify `config.ini` as you see fit. The example file contains documentation
   for pretty much all the configuration keys supported by the various tools.
 - Run the `configure` script in `tools` from your configuration directory. This
   should generate a bunch of json files from your `config.ini` file, as well as
   a makefile.
 - Run `./make.py -j4` to start building, then go grab a coffee. You can also
   call `make` directly, but then you won't have any idea what's going on. The
   Python script writes the build output to `log` and only prints what you need
   to know to the screen.
 - If everything worked out, there should now be an `install` directory in your
   configuration directory.
 - Run the `sourceme` script in the root of the `install` directory to add the
   r-VEX tools to your search path.


Troubleshooting
---------------

If the above didn't work, you're probably missing some dependencies. However,
because the toolchain is usually built using heavy multithreading, it can be
difficult to pinpoint what actually went wrong. Here's some tips for debugging
the builder.

 - Very occasionally, a build may fail due to some race condition. If you get
   weird errors just try again once, or run without parallelism (see below).
 - Add `PARALLEL=` to the make command line to disable multithreaded
   compilation. This should make the errors slightly more easy to find.
 - Look through the generated makefile to learn what the make targets are. Each
   tool has its own target, so if you're stuck on getting some tool to compile,
   you can get make to only recompile that tool every time you retry.
 - If you have reason to mistrust the automatic build management and want to
   clean stuff, you can use the `make clean` command.
 - If stuff *really* broke, you can also delete the `local` and `open64`
   directories in the `tools` directory manually. This will cause everything to
   be redownloaded from the repositories.


Adding to the toolchain builder
-------------------------------

If you want to add something to the toolchain builder, you need to understand
approximately how the process works.

 - The user calls `configure`, which does two things:
    - `Makefile` is generated using the `Makefile.in` template.
    - `<tool>-config.json` is generated from the user's configuration file by
      `config-parse.py`, which uses `meta.ini` as a sort of template.
 - The user calls `make`, which runs the generated `Makefile`. The default
   target causes all the tools to be compiled.
 - Compilation is done using `build.py` for each tool. `build.py` uses
   `tools.json` as a configuration file; this file contains the download and
   build instructions for every tool. `build.py` is what manages intermediate
   product storage, which is nontrivial because the compiler itself changes for
   libraries. It roughly goes through the following steps:
    - "Lock" the tool, to prevent multiple processes from building the same tool
      at the same time. Otherwise build products will get messed up.
    - Download the tool if necessary.
    - Optional: check out a specific commit or branch if requested. It makes
      sure that the repository is left in the original state when it finishes.
    - Optional: clean the build products.
    - Run `rvex-cfg.py` using the supplied json file if the json file or any of
      `rvex-cfg.py`'s dependencies have changed (listed in `rvex-cfg-deps`).
    - Run `configure` if necessary.
    - Run `make` with the specified level of parallelism (-j) and `make install`.
    - Output a "manifest" file in the manifest subdir of the prefix directory.
      This file simply contains a list of all non-symlink files produced by the
      tool. The file is not touched if none of the output files changed. This
      can be used for dependency checking by the toplevel Makefile.
   `build.py` allows you to configure and override all kinds of stuff from the
   command line. If you want to dive into that though, you might have to dive
   into its Python source as well.
 - There's several "phases" of toolchain compilation, because some tools depend
   on others. These dependencies are specified simply using the toplevel Makefile.

So let's say that you want to add a tool. You will need to:

 - Make a repository with your tool. In the root of the repo, there should
   AT LEAST be:
    - `rvex-cfg.py`. Copypaste the file from another tool, then add your own
      configuration code after the "Tool-specific from here onwards" marker.
      You need to keep the `parse_cfg()` call, and you need to figure out for
      yourself which configuration keys (from the user's INI file) your tool
      should depend on. Every such key should be listed here, along with a
      default value in case it isn't specified. The default values should
      correspond to the defaults in `meta.ini`. It is perfectly permissible to
      add configuration keys here that `meta.ini` doesn't (yet) know about, and
      it's also perfectly permissible to leave configuration keys out here.
      However, if `meta.ini` specifies that your tool should listen to some key
      using a `<your-tool> = YES` line, `rvex-cfg.py` will fail if this key
      isn't set to the default value and also isn't specified in the dict passed
      to `parse_cfg()`. This is a compatibility thing; if the user requests
      something from a tool that the tool doesn't know about, it probably can't
      comply.
    - `rvex-cfg-deps`. This file is simply a list of template files that
      `rvex-cfg.py` uses. It's used for determining whether `rvex-cfg.py` needs
      to be re-run.
    - `configure`. You can generate this using `autoconf` or you can copypaste
      one of the much simpler `configure`-like scripts from one of the other
      repositories. It needs to accept `--prefix` and it needs to support
      alternate build directories. At the very least, it needs to generate a
      `Makefile` in the build root that has a default target that builds
      everything and an `install` target that installs the products to the
      `--prefix` directory.
 - In your repository you probably also want:
    - A `.gitignore` file to ignore build products in case the user manually
      configures and builds within the repository.
    - `README.md` containing tool-specific documentation and instructions.
 - Update the tool builder configuration:
    - Add your tool to `tools.json` so `build.py` knows about it.
    - Add your tool to `configure` (the list near the end) to get it to generate
      a json file for it.
    - Add `<tool>` and `clean-<tool>` targets to `Makefile.in` for your tool.
      Make sure that the `all` and `clean` targets depend on these. If your tool
      depends on the build products of other tools, add dependencies that are
      based on the manifest files of those tools, and make sure that make knows
      how to do an initial build of the tool yours depends on if the manifest
      file is missing.
    - If you added configuration keys, update `example-config.ini` accordingly
      (that is, with documentation and the appropriate default values), and
      update `meta.ini` to register them. The `DEFAULT` key in `meta.ini` is
      evaluated as a Python expression that can access the values for keys that
      are defined earler in `meta.ini` through the `d` dict. The result of the
      expression must be the default value that's put in the json files. The
      `CHECK` key is also a Python expression, which is used to parse the user's
      configuration value. The value is passed to the expression as a string in
      `x`. The result should be either the parsed value, or None/an exception to
      indicate misconfiguration. The remainder of the keys specify which tools
      are affected by this key; if a tool does not know about the key and the
      value specified by the user is not the default value, the build will
      intentionally fail to tell the user about this.
