#!/usr/bin/env python3




from select import select
from subprocess import PIPE
from plumbum import local, ProcessExecutionError
from plumbum.commands import ExecutionModifier
from plumbum.lib import read_fd_decode_safely
import re
import collections
import sys
import signal
import os

class Term(object):
    
    def __init__(self):
        self.lines = []
        self.tool_idx = {}
    
    def status(self, tool, status):
        
        # Format the status.
        if status.lower().startswith('error'):
            color = '31;1'
        elif status.lower().startswith('note'):
            color = '33;1'
        elif status.lower().startswith('done'):
            color = '32;1'
        else:
            color = '1'
        status = '\033[%sm%s: %s\033[0m' % (color, tool, status)
        
        # Register it in our line cache.
        idx = self.tool_idx.get(tool, None)
        if idx is None:
            idx = len(self.lines)
            self.tool_idx[tool] = idx
            self.lines.append(status)
            print('')
        else:
            self.lines[idx] = status
        
        # Move cursor up to the line that we need to update.
        back = len(self.lines) - idx
        
        # Update it.
        print('\r\033[%dA\033[K%s\r\033[%dB' % (back, status, back), end='')
        sys.stdout.flush()
    
    def clean_all(self):
        
        # Move the cursor to the start of the status lines and clear all below.
        print('\r\033[%dA\033[K\033[J' % len(self.lines), end='')
        sys.stdout.flush()
    
    def print_all(self):
        for line in self.lines:
            print(line)

interrupted = [False]

class _FILTER(ExecutionModifier):
    
    __slots__ = ("retcode", "buffered", "timeout")
    
    def __init__(self, retcode=0, buffered=True, timeout=None):
        self.retcode = retcode
        self.buffered = buffered
        self.timeout = timeout
    
    def __rand__(self, cmd):
        term = Term()
        with open('log', 'w', 4096) as logf:
            def preexec_function():
                os.setpgrp()
                pass
            with cmd.bgrun(retcode=self.retcode, stdin=None, stdout=PIPE, stderr=PIPE,
                           timeout=self.timeout, preexec_fn=preexec_function) as p:
                
                out = p.stdout
                err = p.stderr
                prev = {out: '', err: ''}
                errored = False
                context = collections.deque(maxlen=50)
                prefix = {out: 'O', err: 'E'}
                logline = 0
                
                def send_sigkill(n, f):
                    interrupted[0] = True
                    signal.signal(signal.SIGINT, signal.SIG_DFL)
                    print('\n\033[1;31mInterrupt received! Sending SIGKILL to process group %d...\033[0m' % p.pid)
                    local['kill']('-9', '-%d' % p.pid)
                    sys.exit(-9)
                
                def send_sigint(n, f):
                    interrupted[0] = True
                    signal.signal(signal.SIGINT, send_sigkill)
                    print('\n\033[1;33mInterrupt received! Sending SIGINT to process group %d...\033[0m\nPress Ctrl+C again to kill.' % p.pid)
                    local['kill']('-2', '-%d' % p.pid)
                
                signal.signal(signal.SIGINT, send_sigint)
                
                while True:
                    try:
                        while p.poll() is None:
                            #print('x')
                            ready, _, _ = select((out, err), (), ())
                            for fd in ready:
                                
                                data, text = read_fd_decode_safely(fd, 4096)
                                if not data:  # eof
                                    continue
                                
                                # Split the received data into lines.
                                text = prev[fd] + text
                                *lines, prev[fd] = text.split('\n')
                                
                                # Output to the log file.
                                for line in lines:
                                    
                                    # Add the prefix.
                                    line = '%s | %s\n' % (prefix[fd], line)
                                    
                                    # Write to the output file.
                                    logf.write(line)
                                
                                # Output to stdout if deemed important enough.
                                for line in lines:
                                    logline += 1
                                    
                                    # Handle printing the first error.
                                    if not errored and not interrupted[0]:
                                        context.append(line)
                                        
                                        if re.search(r'[Ee]rror \d(?! \([Ii]gnored\))', line):
                                            errored = True
                                            
                                            # Dump the context of the error.
                                            term.clean_all()
                                            print("Error! Here's some context, look in the log around line %d for more info." % logline)
                                            for line in context:
                                                print('| ', line)
                                            print()
                                            term.print_all()
                                    
                                    # Handle printing status updates.
                                    if line.startswith('###build### '):
                                        try:
                                            tool, status = line[12:].split(': ', 1)
                                            if not interrupted[0]:
                                                term.status(tool, status)
                                        except ValueError:
                                            pass
                    except (InterruptedError, KeyboardInterrupt):
                        continue
                    break
                
                return p.returncode

FILTER = _FILTER()

try:
    cmd = local['make']
    for a in sys.argv[1:]:
        cmd = cmd[a]
    cmd & FILTER
except ProcessExecutionError as e:
    if not interrupted[0]:
        print('Error: ', e)
    sys.exit(e.retcode)
