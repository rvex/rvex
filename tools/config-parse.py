#!/usr/bin/env python3

import configparser
import sys
import json
import os
from collections import OrderedDict

# Configuration file. There should be an adequately documented template file
# near this script.
config = configparser.ConfigParser()

# Metadata file. This file should contain a section for every entry in
# config.ini by the name [<section>.<item>] in lowercase. These sections
# should contain:
#   DEFAULT: evaluated Python expression with d (see below) as available local
#            that should return the default value for this config entry.
#   CHECK:   evaluated Python expression with x (the config entry value as a
#            string) and d (see below) as available locals that should return
#            the parsed value for this config entry. If the string cannot be
#            parsed or is illegal, either None should be returned or an
#            exception should be thrown.
#   <TOOL>:  YES/NO entry indicating whether the specified tool is affected by
#            this option. If set to NO or omitted, the generated json file
#            will still contain the config entry, but it will always be marked
#            as default so the tool script won't raise an error.
meta = configparser.ConfigParser()

# Dict of parsed keys as passed to eval. Keys are lowercase <section>.<item>,
# added in the order of meta.ini. Values are the parsed values as returned by
# the "check" evaluation.
d = {}

# Dict of data as passed to the tool configuration script. Keys are lowercase
# <section>.<item>. Values are two-tuples. The first entry of the two-tuple
# indicates whether this is a non-default value: if True, a tool configure
# script that doesn't recognize the key must throw an error; if False, it
# should silently ignore it. The second entry is the parsed values as returned
# by the "check" evaluation.
data = OrderedDict()


# Parse command line.
if len(sys.argv) < 2:
    print('Generates a tool config.json configuration file from a central config.ini file.', file=sys.stderr)
    print('', file=sys.stderr)
    print('Usage: %s <tool> [config.ini] [<tool>-config.json] [meta.ini]' % sys.argv[0], file=sys.stderr)
    print('', file=sys.stderr)
    print('config.ini and meta.ini are inputs, <tool>-config.json is the output.', file=sys.stderr)
    print('config.ini defaults to being in the working dir, meta.ini defaults to', file=sys.stderr)
    print('being in the script dir.', file=sys.stderr)
    sys.exit(2)

tool = sys.argv[1]
config_fname = sys.argv[2] if len(sys.argv) > 2 else 'config.ini'
out_fname    = sys.argv[3] if len(sys.argv) > 3 else tool + '-config.json'
meta_fname   = sys.argv[4] if len(sys.argv) > 4 else os.path.dirname(os.path.realpath(__file__)) + os.sep + 'meta.ini'

# Read ini files.
if not os.path.exists(config_fname):
    print('Configuration file not found: ' + config_fname, file=sys.stderr)
    sys.exit(2)
config.read(config_fname)

if not os.path.exists(meta_fname):
    print('Metadata file not found: ' + meta_fname, file=sys.stderr)
    sys.exit(2)
meta.read(meta_fname)

def parse_size(s):
    s = s.lower()
    if s.endswith('g'):
        return int(s[:-1], 0) * 1024 * 1024 * 1024
    elif s.endswith('m'):
        return int(s[:-1], 0) * 1024 * 1024
    elif s.endswith('k'):
        return int(s[:-1], 0) * 1024
    else:
        return int(s[:-1], 0)

# Parse the files.
for key in meta.sections():
    key_meta = meta[key]
    
    # Evaluate the default value.
    default = key_meta.get('DEFAULT')
    try:
        default = eval(default, {}, {'d': d, 'parse_size': parse_size})
    except:
        print('While parsing "%s" using `%s`:' % (key, default), file=sys.stderr)
        raise
    
    # Get config.ini raw value.
    value = config.get(*key.rsplit('.', 1), fallback=None)
    
    # Use the default value if no value is specified.
    if value is None:
        value = default
    else:
        
        # Evaluate the actual value.
        try:
            check = key_meta.get('CHECK')
            checked_value = eval(check, {}, {'d': d, 'x': value, 'parse_size': parse_size})
        except:
            print('While parsing "%s" using `%s`:' % (key, check), file=sys.stderr)
            raise
        if checked_value is None:
            print('While parsing "%s":' % key, file=sys.stderr)
            print('"%s" is an illegal value' % value, file=sys.stderr)
            sys.exit(1)
        value = checked_value
    
    # Save the value in the d dict for usage by later evaluations.
    d[key] = value
    
    # Figure out whether the tool should throw an error if it does not
    # recognize this value.
    if key_meta.get(tool.upper(), 'NO').upper() == 'YES':
        required = value != default
    else:
        required = False
    
    # Add the tool json file entry.
    data[key] = {'req': required, 'val': value}

# Save the json file.
with open(out_fname, 'w') as f:
    f.write(json.dumps(data, indent=4))
