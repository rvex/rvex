

PREFIX    = @prefix@
BUILDDIR  = @builddir@
SCRIPTDIR = @scriptdir@
CONFIGDIR = $(shell pwd)

# Set this to "-i <release_x_x>" to check out a specific revision when the
# subrepositories are first cloned.
ifeq ($(wildcard $(SCRIPTDIR)/../release-tag),)
  INIT_REVISION =
else
  INIT_REVISION = -i $(shell cat $(SCRIPTDIR)/../release-tag)
endif

MANIFESTS = $(PREFIX)/manifest
BUILD = export PROOT_NO_SECCOMP=1 && mkdir -p $(BUILDDIR) && cd $(BUILDDIR) && $(SCRIPTDIR)/build.py -n auto $(INIT_REVISION)
PARALLEL = -j16

.PHONY: all
all: toolchain libs debug

.PHONY: clean
clean: clean-toolchain clean-libs clean-debug


#==============================================================================
# r-VEX sources + documentation
#==============================================================================

# "Recompiles" the r-VEX. In this context, recompilation refers to the code
# generation step for generated VHDL files and headers, as well as compiling
# the LaTeX sources of the manual.
.PHONY: rvex
rvex:
	$(BUILD) rvex $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/rvex-config.json

#------------------------------------------------------------------------------

# Cleans the rvex.
.PHONY: clean-rvex
clean-rvex:
	rm -rf $(BUILDDIR)/local/rvex/auto.*

#------------------------------------------------------------------------------

# Generate a dependency list for stuff that depends on the rvex. We use
# the manifest files for that. We also include the manifest files themselves
# and create rules for them that build the individual tools, so the rvex
# gets compiled if it hasn't been compiled yet at all if the user only wants
# to build something that depends on it.
RVEX_DEPS  = $(MANIFESTS)/rvex $(patsubst %,$(PREFIX)/%,$(shell cat $(MANIFESTS)/rvex; true))
$(MANIFESTS)/rvex:
	$(MAKE) rvex


#==============================================================================
# Compiler toolchain
#==============================================================================

# Recompiles the toolchain.
.PHONY: toolchain
toolchain: glue open64 vexparse binutils

# Individial targets that force compilation and installation.
.PHONY: glue
glue:
	$(BUILD) glue $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/glue-config.json

.PHONY: open64
open64:
	$(BUILD) open64 $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/vexparse-config.json

.PHONY: vexparse
vexparse:
	$(BUILD) vexparse $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/vexparse-config.json

.PHONY: binutils
binutils:
	$(BUILD) binutils $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/binutils-config.json

#------------------------------------------------------------------------------

# Cleans the toolchain.
.PHONY: clean-toolchain
clean-toolchain: clean-glue clean-open64 clean-vexparse clean-binutils

# Cleans individual toolchain targets.
.PHONY: clean-glue
clean-glue:
	rm -rf $(BUILDDIR)/local/glue/auto.*

.PHONY: clean-open64
clean-open64:
	rm -rf $(BUILDDIR)/open64/open64/auto.*

.PHONY: clean-vexparse
clean-vexparse:
	rm -rf $(BUILDDIR)/local/vexparse/auto.*

.PHONY: clean-binutils
clean-binutils:
	rm -rf $(BUILDDIR)/local/binutils/auto.*

#------------------------------------------------------------------------------

# Generate a dependency list for stuff that depends on the toolchain. We use
# the manifest files for that. We also include the manifest files themselves
# and create rules for them that build the individual tools, so the toolchain
# gets compiled if it hasn't been compiled yet at all if the user only wants
# to build something that depends on it.
TOOLCHAIN_DEPS  = $(MANIFESTS)/glue $(patsubst %,$(PREFIX)/%,$(shell cat $(MANIFESTS)/glue; true))
$(MANIFESTS)/glue:
	$(MAKE) glue

TOOLCHAIN_DEPS += $(MANIFESTS)/open64 $(patsubst %,$(PREFIX)/%,$(shell cat $(MANIFESTS)/open64; true))
$(MANIFESTS)/open64:
	$(MAKE) open64

TOOLCHAIN_DEPS += $(MANIFESTS)/vexparse $(patsubst %,$(PREFIX)/%,$(shell cat $(MANIFESTS)/vexparse; true))
$(MANIFESTS)/vexparse:
	$(MAKE) vexparse

TOOLCHAIN_DEPS += $(MANIFESTS)/binutils $(patsubst %,$(PREFIX)/%,$(shell cat $(MANIFESTS)/binutils; true))
$(MANIFESTS)/binutils:
	$(MAKE) binutils


#==============================================================================
# Libraries
#==============================================================================

# Recompiles the libraries.
.PHONY: libs
libs: newlib libgcc drivers

# Recompiles newlib.
NEWLIB_FLAGS  = -t $(CONFIGDIR)/newlib-config.json
NEWLIB_FLAGS += -E "PATH=$(PREFIX)/bin:$$PATH"
NEWLIB_FLAGS += -O newlib/libc/machine/rvex/machine/rvex.h=$(PREFIX)/rvex-elf32/include/rvex.h
BUILD_NEWLIB = $(BUILD) newlib $(PREFIX) $(PARALLEL) $(NEWLIB_FLAGS)
.PHONY: newlib newlib-only
newlib: toolchain $(RVEX_DEPS)
newlib-only: glue
newlib newlib-only:
	$(MAKE) $(MANIFESTS)/newlib-clean
	@if [ -e "$(MANIFESTS)/newlib-clean-pending" ]; then \
	  echo "Clean + build newlib..."; \
	  $(BUILD_NEWLIB) -c; \
	else \
	  echo "Rebuild newlib..."; \
	  $(BUILD_NEWLIB); \
	fi
	rm -f $(MANIFESTS)/newlib-clean-pending

# Clean newlib every time the toolchain executables are modified.
$(MANIFESTS)/newlib-clean: $(TOOLCHAIN_DEPS)
	touch $@-pending
	touch $@

# Recompiles libgcc.
LIBGCC_FLAGS  = -t $(CONFIGDIR)/libgcc-config.json
LIBGCC_FLAGS += -E "PATH=$(PREFIX)/bin:$$PATH"
BUILD_LIBGCC = $(BUILD) libgcc $(PREFIX) -j1 $(LIBGCC_FLAGS)
.PHONY: libgcc libgcc-only
libgcc: toolchain
libgcc libgcc-only:
	$(MAKE) $(MANIFESTS)/libgcc-clean
	@if [ -e "$(MANIFESTS)/libgcc-clean-pending" ]; then \
	  echo "Clean + build libgcc..."; \
	  $(BUILD_LIBGCC) -c; \
	else \
	  echo "Rebuild libgcc..."; \
	  $(BUILD_LIBGCC); \
	fi
	rm -f $(MANIFESTS)/libgcc-clean-pending

# Clean libgcc every time the toolchain executables are modified.
$(MANIFESTS)/libgcc-clean: $(TOOLCHAIN_DEPS)
	touch $@-pending
	touch $@

# Recompiles drivers. FIXME: currently this must always be done after newlib is
# compiled to get a sane build, because newlib spams some placeholder header
# files that the drivers repository overrides. This behavior would be difficult
# to fix in newlib without mutilating machine-independent code. The
# newlib-intended way to fix this would be to define a "system" for r-VEX, but
# I haven't had the time to figure out how to do that yet. If you're going to,
# by the way, there's also some syscall definitions in libgloss that belong
# there.
DRIVER_FLAGS  = -t $(CONFIGDIR)/drivers-config.json
DRIVER_FLAGS += -E "PATH=$(PREFIX)/bin:$$PATH"
BUILD_DRIVERS = $(BUILD) drivers $(PREFIX) $(PARALLEL) $(DRIVER_FLAGS)
.PHONY: drivers drivers-only
drivers: newlib
drivers drivers-only:
	$(MAKE) $(MANIFESTS)/drivers-clean
	@if [ -e "$(MANIFESTS)/drivers-clean-pending" ]; then \
	  echo "Clean + build drivers..."; \
	  $(BUILD_DRIVERS) -c; \
	else \
	  echo "Rebuild drivers..."; \
	  $(BUILD_DRIVERS); \
	fi
	rm -f $(MANIFESTS)/drivers-clean-pending

# Clean drivers every time the toolchain executables are modified.
$(MANIFESTS)/drivers-clean: $(TOOLCHAIN_DEPS)
	touch $@-pending
	touch $@


#------------------------------------------------------------------------------

# Cleans the libraries.
.PHONY: clean-libs
clean-libs: clean-newlib clean-libgcc clean-drivers

# Cleans individual libraries.
.PHONY: clean-newlib
clean-newlib:
	rm -rf $(BUILDDIR)/local/newlib/auto.*

.PHONY: clean-libgcc
clean-libgcc:
	rm -rf $(BUILDDIR)/local/libgcc/auto.*

.PHONY: clean-drivers
clean-drivers:
	rm -rf $(BUILDDIR)/local/drivers/auto.*


#==============================================================================
# Debugging tools
#==============================================================================

# Recompiles the debug tools.
.PHONY: debug
debug: simrvex rvd

# Recompiles simrvex.
.PHONY: simrvex
simrvex:
	$(BUILD) simrvex $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/simrvex-config.json

# Recompiles rvd etc.
.PHONY: rvd
rvd:
	$(BUILD) rvd $(PREFIX) $(PARALLEL) -t $(CONFIGDIR)/rvd-config.json

#------------------------------------------------------------------------------

# Cleans the debug tools.
.PHONY: clean-debug
clean-debug: clean-simrvex clean-rvd

# Cleans individual debug tools.
.PHONY: clean-simrvex
clean-simrvex:
	rm -rf $(BUILDDIR)/local/simrvex/auto.*

.PHONY: clean-rvd
clean-rvd:
	rm -rf $(BUILDDIR)/local/rvd/auto.*

