#!/bin/bash

# This "configure" file configures a full r-VEX toolchain build.

here=`pwd`

scriptdir=`dirname $0`
scriptdir=`realpath $scriptdir`

# The directory that contains build.py's repository. You'll typically only want
# one of these, so it defaults to the location of the configure script.
builddir=$scriptdir

# The directory that contains the intermediate configuration files and the
# generated Makefile. This defaults to where you invoked this script from.
cfgdir=$here

# The primary config.ini file to use. This defaults to $cfgdir/config.ini.
cfgfile=

# The directory that you want to install to. This defaults to an `install`
# directory in the current working directory, but if you like, you should also
# be able to install to / or /usr without destroying your system. No guarantees
# though.
prefix=$here/install

args=()
prev=
for option in "$@"
do
  # If the previous option needs an argument, assign it.
  if test -n "$prev"; then
    eval "$prev=\$option"
    prev=
    continue
  fi

  optarg=`expr "x$option" : 'x[^=]*=\(.*\)'`

  case $option in

  -builddir | --builddir | --builddi | --buildd | --build | --buil | --bui | --bu | --b )
    prev=builddir ;;
  -builddir=* | --builddir=* | --builddi=* | --buildd=* | --build=* | --buil=* | --bui=* | --bu=* | --b=* )
    builddir=$optarg ;;

  -cfgdir | --cfgdir | --cfgdi | --cfgd )
    prev=cfgdir ;;
  -cfgdir=* | --cfgdir=* | --cfgdi=* | --cfgd=* )
    cfgdir=$optarg ;;

  -cfgfile | --cfgfile | --cfgfil | --cfgfi | --cfgf | --cfg | --cf | --c )
    prev=cfgfile ;;
  -cfgfile=* | --cfgfile=* | --cfgfil=* | --cfgfi=* | --cfgf=* | --cfg=* | --cf=* | --c=* )
    cfgfile=$optarg ;;

  -prefix | --prefix | --prefi | --pref | --pre | --pr | --p )
    prev=prefix ;;
  -prefix=* | --prefix=* | --prefi=* | --pref=* | --pre=* | --pr=* | --p=* )
    prefix=$optarg ;;

  *)
    args+=("$option") ;;

  esac
done

if test -n "$prev"
then
  option=--`echo $prev | sed 's/_/-/g'`
  echo "error: missing argument to $option" >&2
  exit 1
fi

# Default configuration file.
if ! test -n "$cfgfile"
then
  cfgfile=$cfgdir/config.ini
fi

cfgfile=`realpath $cfgfile`

for var in builddir cfgdir prefix
do
  eval val=$`echo $var`
  case $val in
    [\\/$]* | ?:[\\/]* | NONE | '' )
      ;;
    *)
      echo "error: expected an absolute directory name for --$var: $val" >&2
      exit 1 ;;
  esac
done

# Create the <tool>-config.json files.
for tool in rvex glue open64 vexparse binutils libgcc newlib uclibc simrvex rvd drivers
do
  echo "Configuring $tool..."
  "$scriptdir/config-parse.py" "$tool" "$cfgfile" "$cfgdir/$tool-config.json" "$scriptdir/meta.ini" || exit 1
done

# Build our own makefile.
sed "s,@prefix@,$prefix,;s,@builddir@,$builddir,;s,@scriptdir@,$scriptdir," "$scriptdir/Makefile.in" > "$cfgdir/Makefile"

# Copy the build.py filter script.
cp -t "$cfgdir" "$scriptdir/make.py"

