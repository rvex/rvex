
# This is an example config.ini file for the configure script in this
# directory. All configuration keys are commented out to let the procedural
# default values take effect. The values listed for the commented keys are the
# "default" default values; that is, if you would uncomment all of the keys the
# result would be the same, but as soon as you modify one of the values the
# defaults for keys specified further down into the file may change (and the
# listed values may become invalid due to mutually exclusive settings).

#==============================================================================
# r-VEX functional resources
#==============================================================================
[resources]

# Number of pipelanes. Defaults to 8.
#NUM_LANES       = 8

# Number of reconfiguration groups. If set to 1, the r-VEX is not
# reconfigurable. The size of a group must be at least 2 lanes, so the maximum
# is NUM_LANES / 2. Defaults to the maximum to get a reconfigurable core.
#NUM_GROUPS      = 4

# The number of hardware contexts. You usually want this to be >= NUM_GROUPS,
# otherwise this value limits the number of threads that can run
# simultaneously. Defaults to NUM_GROUPS.
#NUM_CONTEXTS    = 4

# The minimum boundary that the r-VEX expects all bundles to be aligned to,
# defined as a number of syllables (so 2 means 8-byte alignment). This controls
# the complexity of the instruction compression logic. This cannot be greater
# than NUM_LANES, and you typically will want it to be at most
# NUM_LANES/NUM_GROUPS to get decent narrow-issue generic binary performance.
# This cannot easily be set to 1, because branch offsets are by default encoded
# in 8-byte units. Defaults to 2.
#BUNDLE_ALIGN    = 2

# Long immediate configuration. One or both of these must be enabled.
# limmh_prev is only supported when instruction compression is disabled, i.e.
# if BUNDLE_ALIGN == NUM_LANES. Therefore, the default is to only have
# limmh_neighbor, which is fine for generic binaries.
#LIMMH_PREV      = NO
#LIMMH_NEIGHBOR  = YES

# The following defs control which functional units are available for which
# lanes. ALUs are available everywhere. The first digit maps to lane 0.
# Defaults to an integer multiplier for every lane and no floating point units.
#INT_MUL_LANES   = 11111111
#FLOAT_ADD_LANES = 00000000
#FLOAT_MUL_LANES = 00000000
#FLOAT_CMP_LANES = 00000000
#FLOAT_I2F_LANES = 00000000
#FLOAT_F2I_LANES = 00000000

# Memory unit configuration. The amount of physical memory units is hardcoded
# to one per lane group; CFG_MEM_LANE specifies the offset relative to the
# first lane in each group. For instance, NUM_LANES=8, NUM_GROUPS=4, and
# CFG_MEM_LANE=1 would result in 01010101 in the format used above. Defaults to
# 0 for the first/even lanes.
#MEM_LANE        = 0

# It is usually illegal for the r-VEX to process multiple memory operations in
# the same cycle within the same context, as the data cache is limited to this.
# However, if you're interfacing with the r-VEX directly and know there is no
# such constraint in your memory system, you can increase the number of memory
# resources here. Defaults to YES.
#MEM_ONE_PER_CYC = YES

#==============================================================================
# Memory layout
#==============================================================================
[memory]

# Data memory layout. If the r-VEX has a unified data/instruction memory, this
# defines the unified memory block. DATA_BASE_RVEX specifies the base address
# of the block from the perspective of the r-VEX (default 0). DATA_BASE_DBG
# specifies the base address from the perspective of rvsrv/rvd (if not
# specified, it defaults to DATA_BASE_RVEX). DATA_SIZE specifies the size of
# the block (default 512MiB).
#DATA_BASE_RVEX  = 0x00000000
#DATA_BASE_DBG   = 0x00000000
#DATA_SIZE       = 512M

# .stack and .heap section sizes. Default 16kiB.
#STACK_SIZE      = 16k
#HEAP_SIZE       = 16k

# Instruction memory layout. Only define these if the r-VEX has a separate
# instruction and data memory. The keys are the same as for the data memory.
#INSN_BASE_RVEX  = 0x00000000
#INSN_BASE_DBG   = 0x00000000
#INSN_LIZE       = 32k

# r-VEX control register base address. CTRL_BASE_RVEX specifies the base
# address from the perspective of the r-VEX; this is the value of
# CFG.cregStartAddress in VHDL. CTRL_BASE_DBG specifies the base address from
# the perspective of rvsrv/rvd.
#CTRL_BASE_RVEX  = 0xFFFFFC00
#CTRL_BASE_DBG   = 0xF0000000

#==============================================================================
# r-VEX debug features
#==============================================================================
[debug]

# Number of hardware breakpoints supported. Max and default is 4, set to 0 to
# disable if you don't need them.
#NUM_BREAKPOINTS = 4

# Controls whether the hardware trace unit is instantiated. This unit allows
# the r-VEX to generate a bytestream that the program `rvtrace` can convert
# into an instruction trace along with a disassembly file similar to what
# `simrvex` outputs. However, it requires a signigicant amount of hardware (and
# slows the core down a lot when also enabled at runtime).
#TRACE_UNIT      = NO

# Width of the per-context performance counters. Max is 7, default is 4, set to
# 0 to disable if you don't need them.
#PERF_CNT_WIDTH  = 4

#==============================================================================
# Toolchain configuration
#==============================================================================
[toolchain]

# Controls whether the tools will generate generic binaries or not. This
# defaults to YES if the core is configured to be reconfigurable and to NO if
# not. If you override this to NO for a reconfigurable core, the compiled code
# will only run correctly when the core is running in its max-issue
# configuration; if you want to generate code for smaller sizes, you will need
# to generate a different toolchain (just decrease the lane count in that
# case). If you override this to YES for a non-reconfigurable core, the code
# will run on other core sizes as well, as long as the lane configurations are
# compatible.
#GENERIC_BINARY  = YES

# Controls whether the assembler autosplit function is enabled or disabled by
# default. This feature tries to split up bundles that cannot be scheduled in
# a single cycle instead of raising an error. It defaults to YES if
# GENERIC_BINARY is YES (implicitly or explicitly). It's currently not
# implemented when not compiling generic binaries.
#AUTOSPLIT       = YES

# Controls whether vexparse is to be used to optimize/reschedule assembly code
# before it's passed to the assembler, and if so, its optimization level. Like
# autosplit, it only works for generic binaries. If this is NO (the default),
# vexparse is disabled entirely and not included in the build. The other legal
# values are 0 through 2, which enable vexparse by default, using -O0 through
# -O2 respectively. vexparse can fix some things that autosplit cannot, and
# generally produces faster code, but compilation can be excessively slow
# (especially for large basic blocks), and on rare occasions it produces
# incorrect code due to some bugs we haven't found yet.
#VEXPARSE_OPT    = NO

# Vexparse currently does not properly support non-default pipeline
# configurations, in the sense that not all parts of the code properly check
# the configured latencies. Therefore, vexparse will refuse to build for
# non-default pipelines. If you want to override that for experimentation or
# whatever, set this to YES.
#VEXPARSE_UNSTABLE = NO

#==============================================================================
# Software/driver/runtime configuration
#==============================================================================
[software]

# Selects the runtime to use. Currently this can be:
#  - BARE (default): no C library, just a basic start file that initializes the
#    stack and clears the .bss section. This is good for standalone r-VEXs with
#    very limited memory.
#  - NEWLIB: enables the newlib C library. Requires at least around 16kiB stack
#    and heap, as well as a significant amount of instruction memory.
#RUNTIME         = BARE

#------------------------------------------------------------------------------
# crt0 configuration (bare runtime only)
#------------------------------------------------------------------------------
[software.crt0]

# Disables the .bss section clearing code. This is primarily useful for
# programs that will be run using modelsim; in this case the memory will
# already be initialized with zero and the additional runtime of the (useless)
# .bss clear is only annoying.
#DISABLE_BSS_CLEAR = NO

# Disables the register file clearing code. In theory, the initial contents of
# the register file are considered to be undefined, but having a clean register
# file at startup can aid in debugging. Disabling it primarily saves a bit of
# instruction memory.
#DISABLE_REGFILE_CLEAR = NO

# Disables trap handling and forwarding to a user-supplied interrupt()
# function. This primarily saves instruction memory.
#DISABLE_INTERRUPTS = NO

# Disables a couple of putc calls in the panic handler to save a bit of
# instruction memory if you don't care (or don't have a putc() function).
#DISABLE_PANIC   = NO

# Disables the argc/argv initialization section. This saves a bit of data
# memory, but prevents simrvex from passing argc/argv to the program.
#DISABLE_ARGV    = NO

#------------------------------------------------------------------------------
# Syscall configuration (newlib runtime only)
#------------------------------------------------------------------------------
[software.syscalls]

# This set of keys allows you to disable syscalls you don't need to reduce code
# size. It is not *guaranteed* that a syscall will actually return ENOSYS (or
# actually be disabled) if it is disabled here, though.
#SETUP           = YES
#EXIT            = YES
#READ            = YES
#WRITE           = YES
#OPEN            = YES
#CLOSE           = YES
#LSEEK           = YES
#STAT            = YES
#FSTAT           = YES
#IOCTL           = YES
#FCNTL           = YES
#LINK            = YES
#UNLINK          = YES
#MKDIR           = YES
#RENAME          = YES
#FORK            = YES
#EXECVE          = YES
#WAITPID         = YES
#KILL            = YES
#GETTIMEOFDAY    = YES
#TIMES           = YES
#SBRK            = YES
#IRQ             = YES
#PANIC           = YES
#GETPDATA        = YES

#------------------------------------------------------------------------------
# libfs configuration (newlib runtime only)
#------------------------------------------------------------------------------
[software.libfs]

# Specifies the number of files that can be opened simultaneously, including
# stdin/stdout/stderr. Defaults to 16.
#MAX_FD          = 16

# Specifies whether the r-VEX UART driver should be enabled (it is by default).
# Disable this to save instruction memory if you don't need it.
#ENABLE_RVEX_UART = YES

# Specifies whether the /dev/mem-like driver should be enabled (it is by
# default). Disable this to save instruction memory if you don't need it.
#ENABLE_DEVMEM   = YES

#==============================================================================
# r-VEX special features
#==============================================================================
[special]

# Controls whether forwarding logic is instantiated. Forwarding allows
# results of instructions to be used by subsequent instructions before they are
# committed to the register file. This is the default behavior. It only makes
# sense to disable this if you're aggressively trying to reduce area or improve
# the clock frequency.
# WARNING: non-default instruction latencies are NOT supported by all parts of
# the toolchain. Specifically, the vexparse generic binary rescheduler and
# (possibly) --autosplit don't work right, so you currently can't use generic
# binaries without forwarding.
#DISABLE_FORWARD = NO

# Controls whether traps are supported. This is the default. It only makes
# sense to disable this if you're aggressively trying to reduce area or improve
# the clock frequency.
# WARNING: behavior of the r-VEX is undefined when trap support is disabled and
# there would otherwise have been a trap. Disable at your own risk!
#DISABLE_TRAPS   = NO

# This setting should be used in conjunction with the extended r-VEX pipeline
# (see config/pipeline_extended.ini). It can be used in conjunction with
# FORWARDING=NO, TRAP_SUPPORT=NO, NUM_LANES=2, NUM_GROUPS=1, NUM_CONTEXTS=1,
# NUM_BREAKPOINTS=0, TRACE_UNIT=NO, and PERF_CNT_WIDTH=0 to make a very
# simplistic r-VEX core which is relatively suitable for many-core image
# processing. Note that despite the core being only 2-issue, a lot of ILP (~12)
# is needed to fill all issue slots due to the long datapath latency.
# WARNING: non-default instruction latencies are NOT supported by all parts of
# the toolchain. Specifically, the vexparse generic binary rescheduler and
# (possibly) --autosplit don't work right, so you currently can't use generic
# binaries with a longer pipeline.
#EXTEND_PIPELINE = NO

