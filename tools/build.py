#!/usr/bin/env python3

import json
import sys
import shlex
import os
import hashlib
import datetime
import errno
import time
from plumbum import local, FG, ProcessExecutionError


class BuildError(Exception):
    @classmethod
    def fmt(cls, fmt, *args):
        raise cls('error: ' + (fmt % args))

class DontBuild(Exception):
    @classmethod
    def fmt(cls, fmt, *args):
        raise cls('note: ' + (fmt % args))

def readconfig():
    """Reads the `tools.json` file."""
    
    with open(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'tools.json', 'r') as f:
        return json.load(f)


def lockfile(status, filename):
    #def status(s):
    #    print('%d @ %s: %s' % (os.getpid(), time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()), s))
    #    sys.stdout.flush()
    
    status('acquiring lockfile %s...' % os.path.realpath(filename))
    wait_print = 0
    while True:
        try:
            os.close(os.open(filename, os.O_CREAT | os.O_EXCL | os.O_WRONLY));
        except OSError as e:
            if e.errno == errno.EEXIST:
                if wait_print == 0:
                    status('waiting for %s' % os.path.realpath(filename))
                    wait_print = 9
                else:
                    wait_print -= 1
                time.sleep(1)
                continue
            else:
                raise
        status('acquired %s!' % os.path.realpath(filename))
        return os.path.realpath(filename)


def cmd(fmt, *args, redir=False):
    """
    Runs the given shell command using plumbum. The command is constructed
    using % for easy formatting. - and @ can be specified at the start of the
    command as modifiers, with the same semantics as make. If both are
    specified, the order must be @-cmd."""
    
    # Format.
    c = fmt % args
    
    # Parse and remove the modifiers.
    silent = c.startswith('@')
    if silent:
        c = c[1:]
    ignore = c.startswith('-')
    if ignore:
        c = c[1:]
    
    # Print the command if the @ modifier is not present.
    if not silent:
        print(c)
        sys.stdout.flush()
    
    # Run the program.
    c = shlex.split(c)
    prog = local
    for arg in c:
        prog = prog[arg]
    try:
        if redir:
            return prog()
        else:
            prog & FG
    except ProcessExecutionError as e:
        if ignore:
            print('Error %d (ignored)' % e.retcode)
            sys.stdout.flush()
        else:
            print('Error %d' % e.retcode, file=sys.stderr)
            sys.stderr.flush()
            BuildError.fmt('Error %d (%s ...)', e.retcode, c[0])
        return e.retcode
    
    return 0


def cd(dir):
    print('cd ' + dir)
    sys.stdout.flush()
    local.cwd.chdir(dir)

def get_buildid(config=[], env=[], makevars=[]):
    buildid = '@@@'.join(sorted(config))
    buildid += '@-@' + '@@@'.join(sorted(map(lambda x: '@'.join(x), env)))
    buildid += '@-@' + '@@@'.join(sorted(map(lambda x: '@'.join(x), makevars)))
    h = hashlib.new('sha1')
    h.update(buildid.encode('UTF-8'))
    return h.hexdigest()

def build(rootfs, tool, target, toolcfg=None, config=[], env=[], makevars=[], makeflags=[], files=[], revision=None, init_revision=None, configdir='default', clean=False, pull=False, clean_only=False):
    """
    Builds the given tool with the given config as intelligently as
    possible.
    
    `rootfs` and `tool` represent the tool to build. They must exist in the
    `tools.json` file in the current working directory, as follows:
    
        {
          ...,
          "<rootfs>": {
            "proot": "<curlable URL for .tar.bz2 file containing root FS>",
            "prootflags": "...",
            "tools": {
              ...,
              "<tool>": {
                "repo": "<GIT repository URL for this tool>"
              },
              ...
            }
          },
          ...
        }
    
    The `"proot"` and `"prootflags"` keys are optional. If specified, `"proot"`
    must contain a `curl`able link to a `.tar.bz2` file which, when unpacked,
    produces a directory with the same name as `rootfs`, containing a root
    filesystem that can be `proot`ed into. The tool will then be built within
    this environment. If the key is not specified, the tool will be built in
    the host environment.
    
    `rootfs` and `tool` map to a directory relative to the working directory,
    namely `<rootfs>/<tool>`. If this directory does not exist yet, it is
    created. The GIT repository belonging to this tool, read from the
    `tools.json` file, is cloned out within this directory. Thus, we have the
    following directory structure.
    
        <rootfs>
         '- <tool>
             '- <repo name>
                 |- .git
                 |   '- ...
                 |- configure
                 '- ...
    
    `target` specifies the directory which the output product should be copied
    to.
    
    `config` should be an iterable of configure command line option strings.
    These options are simply joined when passed to configure, but are sorted
    before generating the unique build ID.
    
    `env` should be an iterable of two-tuples representing environment
    variables to be passed to configure and make.
    
    `makevars` works like `env`, but is passed to `make` and `make install`
    as part of its command line, thus overriding variables instead of providing
    defaults.
    
    `makeflags` should be an iterable of strings that are passed as flags to
    `make` (not `make install`). These flags do not affect the build ID. This
    is primarily intended for flags like `-j` that affect performance or error
    level, but not the end result of the build.
    
    `files` should be an iterable of two-tuples representing file overrides.
    The first tuple entry is the destination file relative to the source tree
    root, the second tuple entry is the source file relative to the working
    directory. It is also possible to delete files in the tree by setting the
    second tuple entry to `None`.
    
    `revision` specifies the git revision to use when building. When `None`,
    the working tree is used.
    
    `configdir` optionally specifies an identification string for the
    `config-*` directory. In addition to idenfitication, this allows multiple
    nonconflicting builds to be saved or done in parallel.
    
    `clean` specifies whether intermediate files should be removed prior to the
    build.
    
    `pull` causes the GIT repository to be updated with `git pull` prior to
    the build. The repository is still reverted to the old state after the
    build.
    
    The `config`, `env`, and `makevars` entries are used to generate a unique
    SHA1 ID. This ID is used to determine which build directory to use. The
    following directory structure is used:
    
        <rootfs>
         |- <tool>
         |   |- <repo name>
         |   |   '- ...
         |   |- <configdir>-<ID>
         |   |   |- build         - configure working directory
         |   |   |   |- build.sh  - generated build script
         |   |   |   '- ...       - files generated by build
         |   |   |- install       - install prefix
         |   |   |   '- ...       - install tree
         |   |   '- complete      - file indicating that the build is complete
         |   '- lockfile          - existence indicates that a build is in progress
         |- <bin>                 - existence indicates that the rootfs has been downloaded
         |   '- ...
         |- ...                   - other tools
         '- lockfile              - existence indicates that the rootfs is being downloaded
    
    If the actual build directory is fixed (`"builddir"` key in `tools.json`
    is specified), this directory is emptied prior to building (barring the
    `"builddirignore"` files list), then the contents of the
    `<configdir>-<ID>/build` directory are moved into that directory, then
    `build.sh` is executed from there, and afterwards the contents of the fixed
    build directory are moved back into the configuration-specific directory.
    
    The build process is as follows. If there are any errors, the process is
    stopped immediately, barring any pending cleanup actions.
    
     - The `<rootfs>/<tool>` directory is created if it does not exist yet.
    
     - If the `"proot"` key in `tools.json` exists for this `rootfs`:
     
         - The `<rootfs>/lockfile` is acquired. This prevents parallel builds
           from trying to download and extract the root filesystem
           simultaneously. The lock file should be removed even if the download
           or extraction fails.
         
         - If the `rootfs/bin` directory does not exist, the URL specified by
           the `"proot"` key is downloaded using `curl`. It is then extracted
           using `tar` and finally deleted after extraction.
           
         - The `rootfs/lockfile` is removed.
    
     - `<rootfs>/<tool>/lockfile` is acquired. This prevents parallel builds
       from messing each other up, as the source tree is possibly temporarily
       modified by the build. The lock file should always be removed upon
       completion, regardless of any errors that may have occurred.
    
     - If `<rootfs>/<tool>/<repo name>/.git` already exists and either `pull`
       or `revision` is specified, the following actions are performed relative
       to `<rootfs>/<tool>/<repo name>` to prevent loss of user data:
       
         - Create a clean `tmp` directory next to the repo:

               rm -rf ../tmp
               mkdir -p ../tmp
           
         - Reset the repository (checked-in but uncommitted changes are not
           checked back in):

               git reset
           
         - Get a list of all modified and not-checked-in files:
         
               git ls-files -mo > ../tmp/modified
               git ls-files -i --exclude-standard >> ../tmp/modified
           
         - Move these modified files into `../tmp`, named using the numeric
           index within the list of modified files.
           
         - Get a list of all deleted files:

               git ls-files -d > ../tmp/deleted
           
         - Get the name of the current branch and write it to `../tmp/branch`,
           or write an empty file if the HEAD is detached.
           
         - Get the SHA1 of the current revision and write it to `../tmp/rev`.
           
         - At this point, everything that's important is backed up. Now we just
           need to get the repo in a defined state:

               git reset --hard
               git clean -fdx
          
     - If `pull` is set and `<rootfs>/<tool>/<repo name>/.git` exists,
       `git pull` is executed.
       
     - If `<rootfs>/<tool>/<repo name>/.git` does not exist yet,
       `git clone <repo>` is executed relative to `<rootfs>/<tool>` to download
       it.
     
     - If `revision` is not `None`, `git checkout <revision>` is executed
       relative to `<rootfs>/<tool>/<repo name>`. If `pull` is also set,
       a second `git pull` is performed to update the specified branch to
       `origin/<branch>`.
     
     - Any file overrides specified by `files` are performed by `cp` and any
       deletions are performed by `rm -f`.
     
     - The build ID is computed and the `<rootfs>/<tool>/<configdir>-<ID>`
       directory is created if it doesn't already exist.
     
     - The `<rootfs>/<tool>/<configdir>-<ID>/complete` file is removed if it
       exists.
    
     - If `clean` is set, the `<rootfs>/<tool>/<configdir>-<ID>/build`
       directory is removed if it exists, and then recreated (empty).
       
     - The `<rootfs>/<tool>/<configdir>-<ID>/install` directory is removed if
       it exists, and then recreated (empty).
    
     - A `bash` build script (`build.sh`) is generated within the `build`
       directory that performs, by default, the usual
       `configure; make; make install` combo, but this can be customized using
       the `tools.json` file. It's kind of in flux at the time of writing, so
       check the code for details.
       
     - If the `"builddir"` key is specified, the contents of the specified
       directory are deleted, except for the files listed in
       `"builddirignore"`. The contents of the
       `<rootfs>/<tool>/<configdir>-<ID>/build` directory (including
       `build.sh`) are then moved into the specified build directory.
       
     - The build script is run, wrapped within a `proot` command if the
       `"proot"` key exists within the `tools.json` file. If a `"prootflags"`
       key also exists, its contents are passed as flags to the `proot` call.
     
     - If the `"builddir"` key is specified, the contents of that directory
       are moved to the `<rootfs>/<tool>/<configdir>-<ID>/build`, excluding
       the files and directories listed in `"builddirignore"`.
     
     - The current timestamp is written to the
       `<rootfs>/<tool>/<configdir>-<ID>/complete` file.
     
     - The contents of the `<rootfs>/<tool>/<configdir>-<ID>/install` directory
       are copied to the `<target>` directory.
     
     - Regardless of any previous errors, the following cleanup actions are
       performed (if their counterparts have already been performed):
       
         - Relative to the `<rootfs>/<tool>/<repo name>/` directory:
          
             - If `../tmp` exists, clean the repository;

                   git reset --hard
                   git clean -fdx
               
             - If `../tmp/branch` exists AND is nonempty:

                   git checkout `cat ../tmp/branch`
               
             - If `../tmp/rev` exists and its content do not equal the current
               output of `git rev-parse HEAD`, detach the HEAD by checking out
               the SHA1 directly:

                   git checkout `cat ../tmp/rev`
               
             - If `../tmp/modified` exists, forcibly move all the numbered
               files in `../tmp` back into the repository, generating
               directories if necessary just like `git` would.
               
             - If `../tmp/deleted` exists, remove the files listed in there.
          
         - Release the file lock:
           
               rm -f <rootfs>/<tool>/lockfile
    
    """
    
    def status(s):
        print('###build### %s: %s' % (tool, s))
        sys.stdout.flush()
    
    # PREPARATIONS
    # ============
    
    # Make the target directory absolute and make sure it exists before doing
    # anything else.
    target = os.path.abspath(target)
    cmd('mkdir -p "%s"', target)
    
    # Read the configuration file.
    try:
        config_json = readconfig()
    except (IOError, ValueError) as e:
        BuildError.fmt(
            'Could not read tools.json: %s',
            e)
    
    # Select the configuration node for the selected rootfs.
    config_json = config_json.get(rootfs, None)
    if config_json is None:
        BuildError.fmt(
            'rootfs "%s" not specified in tools.json',
            rootfs)
    
    # Proot key. If specified, this should be a curl'able URL to a .tar.bz2
    # archive containing a rootfs, which must include the proot tool, which
    # will be proot'ed into for the builds.
    proot = config_json.get('proot', None)
    
    # Proot flags key. Contents will be passed to proot.
    prootflags = config_json.get('prootflags', '')
    
    # Get the tool list for this rootfs.
    config_json = config_json.get('tools', None)
    if config_json is None:
        BuildError.fmt(
            'No tool list specified for rootfs "%s" in tools.json',
            tool, rootfs)
    
    # Select the configuration node for the selected tool.
    config_json = config_json.get(tool, None)
    if config_json is None:
        BuildError.fmt(
            'Tool "%s" not specified in rootfs "%s" in tools.json',
            tool, rootfs)
    
    # GIT repository URL for the selected tool. Must be specified.
    repo = config_json.get('repo', None)
    if repo is None:
        BuildError.fmt(
            'No repo specified for tool "%s", rootfs "%s" in tools.json',
            tool, rootfs)
    
    # Configure key. True or False, specifies whether a configure script
    # should be run before make.
    configure       = config_json.get('configure', True)
    
    # Configure script directory key. This is the location of the configure
    # script relative to the fixed build dir if "builddir" is specified, or
    # relative to the repository root otherwise.
    configuredir    = config_json.get('configuredir', '.')
    
    # The format string for the required command line parameters for configure.
    # The default is fine for autoconf-generated configure scripts, but Open64
    # is, as usual, a special little snowflake.
    configureformat = config_json.get('configureformat', '--prefix="{prefix}"')
    
    # The make target for the build. This is usually the default target, but,
    # Open64.
    maketarget      = config_json.get('maketarget', '')
    
    # The make target for installation. For build systems that do both building
    # and installation in one go, this can be set to False to disable the
    # second make command.
    installtarget   = config_json.get('installtarget', 'install')
    
    # If specified, the location of a fixed build directory. autoconf-based
    # systems don't need this because they are be built from whatever the
    # working directory was when configure was called, but some build systems
    # don't have this flexibility.
    builddir        = config_json.get('builddir', None)
    
    # List of top-level (!) files or directories to ignore when moving
    # intermediate files around.
    builddirignore  = config_json.get('builddirignore', [])
    
    # Extract the repository name from the repo URL.
    reponame = repo.split('/')[-1]
    if not reponame.endswith('.git'):
        BuildError.fmt(
            'Repo URL for tool "%s", rootfs "%s" in tools.json does not '
            'end in .git', tool, rootfs)
    reponame = reponame[:-4]
    
    # Make sure that the file overrides are specified as absolute paths and
    # that they all exist.
    files_in = files
    files = []
    for dest, src in files_in:
        if src is not None:
            src = os.path.abspath(src)
            if not os.path.exists(src):
                BuildError.fmt(
                    'File override for "%s", "%s", does not exist',
                    dest, src)
        files.append((dest, src))
    
    # Ensure that the <rootfs>/<tool> directory exists.
    cmd('mkdir -p "%s/%s"', rootfs, tool)
    
    # DOWNLOAD/EXTRACT ROOT FS IF NECESSARY
    # =====================================
    
    if proot is not None:
        
        # Go into the rootfs directory and lock access to it.
        cd(rootfs)
        lockf = lockfile(status, 'lockfile')
        
        try:
            
            # Use the existence of the bin directory to determine whether
            # we've downloaded the build directory yet.
            if not os.path.exists('bin'):
                status('downloading proot environment for ' + rootfs)
                
                # Download the archive.
                cmd('curl %s -o root.tar.bz2', proot)
                
                # Unpack it.
                cmd('tar xjf root.tar.bz2')
                
                # Remove it.
                cmd('rm -f root.tar.bz2')
            
        finally:
            
            # Release the lock to the rootfs directory.
            cmd('rm -f %s' % lockf)
            cd('..')
    
    # TOOL DIRECTORY LOCKING
    # ======================
    
    # Go into the rootfs/tool directory and lock access to it.
    cd(rootfs + os.sep + tool)
    lockf = lockfile(status, 'lockfile')
    
    done_msg = None
    
    try:
        
        # BUILD MANAGEMENT
        # ================
        
        # Generate the SHA1 build ID.
        buildid = get_buildid(config, env, makevars)
        
        # Append it to the configuration directory name.
        configdir += '.' + buildid
        
        # Make sure that the configuration directory exists.
        if not os.path.isdir(configdir):
            cmd('mkdir -p "%s"', configdir)
        
        # Remove the "complete" file if it exists.
        if os.path.exists(configdir + os.sep + 'complete'):
            cmd('rm -f "%s"', configdir + os.sep + 'complete')
        
        # If clean is set, remove the directory with the intermediate products,
        # then recreate it.
        if clean and os.path.exists(configdir + os.sep + 'build'):
            cmd('rm -rf "%s"', configdir + os.sep + 'build')
        if not os.path.exists(configdir + os.sep + 'build'):
            cmd('mkdir -p "%s"', configdir + os.sep + 'build')
        
        # Remove the install directory to remove its contents, then recreate
        # it.
        if os.path.exists(configdir + os.sep + 'install'):
            cmd('rm -rf "%s"', configdir + os.sep + 'install')
        cmd('mkdir -p "%s"', configdir + os.sep + 'install')
        
        # If we're only supposed to clean, stop here.
        if clean_only:
            return
        
        
        # GIT REPOSITORY OPERATIONS
        # =========================
        
        # Back up uncommitted changes and the current branch/revision before
        # doing anything with the repo, then pull it if pull is set.
        if os.path.exists(reponame + os.sep + '.git') and (pull or (revision is not None)):
            
            # Enter the git repository directory.
            cd(reponame)
            try:
                status('cleaning repo (changes will be restored upon completion)')
                
                # Create a clean tmp directory.
                cmd('rm -rf ../tmp')
                cmd('mkdir ../tmp')
                
                # Reset the repository, such that anything checked in but not
                # committed becomes not-checked-in again. Files are not checked
                # back in when the repository is restored.
                cmd('git reset')
                
                # Get a list of all deleted files.
                deleted = cmd('git ls-files -d', redir=True)
                deleted = set(filter(None, deleted.split('\n')))
                with open('../tmp/deleted', 'w') as f:
                    f.write('\n'.join(deleted))
                
                # Get a list of all modified and not-checked-in files.
                modified = ''
                modified += cmd('git ls-files -mo', redir=True)
                modified += cmd('git ls-files -i --exclude-standard', redir=True)
                modified = list(filter(lambda x: x and x not in deleted, modified.split('\n')))
                with open('../tmp/modified', 'w') as f:
                    f.write('\n'.join(modified))
                
                # Move these modified files into `../tmp`, named using the
                # numeric index within the list of modified files.
                for idx, src in enumerate(modified):
                    cmd('mv "%s" ../tmp/%d', src, idx)
                
                # Get the name of the current branch.
                try:
                    branch = cmd('git branch', redir=True)
                    branch = branch.split('\n')
                    branch = list(filter(lambda x: x.startswith('*'), branch))
                    branch = branch[0][1:].strip()
                    if branch.startswith('('):
                        branch = ''
                except IndexError:
                    BuildError.fmt('Failed to get name of current git branch')
                with open('../tmp/branch', 'w') as f:
                    f.write(branch)
                
                # Get the SHA1 of the current revision.
                cur_revision = cmd('git rev-parse HEAD', redir=True)
                cur_revision = cur_revision.strip()
                with open('../tmp/revision', 'w') as f:
                    f.write(cur_revision)
                
                # Bring the repository in a well-defined state.
                cmd('git reset --hard')
                cmd('git clean -fdx')
                
                # If pull is set, pull from the remote.
                if pull:
                    status('updating git repository ' + repo)
                    if revision is not None:
                        cmd('git checkout %s', revision)
                    else:
                        cmd('git checkout master')
                    cmd('git pull')
                
            finally:
                cd('..')
        
        # If the repository does not exist, clone it.
        if not os.path.exists(reponame + os.sep + '.git'):
            status('cloning git repository ' + repo)
            cmd('git clone %s', repo)
            
            if init_revision is not None:
                cd(reponame)
                try:
                    status('checking out initial revision ' + init_revision)
                    cmd('git checkout %s', init_revision)
                finally:
                    cd('..')
        
        # If a specific revision is specified, check out that revision. If
        # pull is also set, this was already done during the updating
        # process.
        if not pull and revision is not None:
            cd(reponame)
            try:
                status('checking out revision ' + revision)
                cmd('git checkout %s', revision)
            finally:
                cd('..')
        
        
        # RVEX-CFG.PY EXECUTION
        # =====================
        
        run_rvex_cfg = False
        if toolcfg:
            
            # See if we need to run rvex-cfg.py.
            toolcfgcopy = configdir + os.sep + 'rvex-cfg.json'
            toolcfgdeps = reponame + os.sep + 'rvex-cfg-deps'
            with open(toolcfg, 'r') as f:
                new = f.read()
            if os.path.exists(toolcfgcopy):
                with open(toolcfgcopy, 'r') as f:
                    old = f.read()
                if new != old:
                    run_rvex_cfg = True
                    status('running rvex-cfg.py (json file changed)')
                if not run_rvex_cfg:
                    deps = ['rvex-cfg.py']
                    if os.path.exists(toolcfgdeps):
                        with open(toolcfgdeps, 'r') as f:
                            deps.extend(filter(bool, map(str.strip, f.read().split('\n'))))
                    for dep in deps:
                        depp = reponame + os.sep + dep
                        if os.path.exists(depp):
                            if os.path.getmtime(depp) > os.path.getmtime(toolcfgcopy):
                                run_rvex_cfg = True
                                status('running rvex-cfg.py (modified dependency %s)' % depp)
                                break
            else:
                run_rvex_cfg = True
                status('running rvex-cfg.py (first build)')
            
            # Run rvex-cfg.py if we need to.
            if run_rvex_cfg:
                cd(reponame)
                try:
                    retval = cmd('-python3 rvex-cfg.py %s', toolcfg)
                    if retval == 3:
                        
                        # Return code 3 means that the configuration is not
                        # erroneous, but the tool shouldn't be built/installed.
                        DontBuild.fmt('Not building %s because its rvex-cfg.py told me not to for %s',
                                    tool, toolcfg)
                        
                    elif retval != 0:
                        BuildError.fmt('Failed to configure tool "%s", rootfs "%s" with "%s"',
                                    tool, rootfs, toolcfg)
                finally:
                    cd('..')
        
            # Save a copy of the json file so we know if we need to run
            # rvex-cfg.py again for the next run. We need to do this after
            # running rvex-cfg.py; otherwise if it crashes we won't run it
            # again the next time the user tries to build.
            if run_rvex_cfg:
                with open(toolcfgcopy, 'w') as f:
                    f.write(new)
        
        
        # FILE OVERRIDES
        # ==============
        
        # Make modifications to the source tree based on the override file
        # list.
        if files:
            status('executing file overrides')
            
            # Create a directory with restoration data.
            cmd('rm -rf tmp-override')
            cmd('mkdir tmp-override')
            
            overridden = []
            added = []
            for dest, src in files:
                
                # Move the overridden file to the temp folder.
                if os.path.exists(reponame + os.sep + dest):
                    cmd('mv -f "%s/%s" "tmp-override/%d"', reponame, dest, len(overridden))
                    overridden.append(dest)
                else:
                    added.append(dest)
                
                # Copy the override file to the repo (unless it is a delete
                # command).
                if src is not None:
                    cmd('cp -rf "%s" "%s/%s"', src, reponame, dest)
            
            # Write index files.
            with open('tmp-override/overridden', 'w') as f:
                f.write('\n'.join(overridden))
            with open('tmp-override/added', 'w') as f:
                f.write('\n'.join(added))
        
        
        # BUILD SCRIPT GENERATION
        # =======================
        
        with open(configdir + os.sep + 'build' + os.sep + 'build.sh', 'w') as f:
            #
            # Shebang and error handling.
            f.write('#!/bin/bash\n')
            f.write('set -e\n')
            
            # Environment setup.
            for key, value in env:
                f.write('export %s=%s\n\n' % (key, value))
            
            # Figure out the prefix directory.
            if proot is None:
                prefix = os.path.abspath(configdir + os.sep + 'install')
            else:
                prefix = '/%s/%s/install' % (tool, configdir)
            
            # Configure command.
            if configure:
                
                # We need to reconfigure when any part of the configure command
                # line changes, but we don't want to reconfigure every time,
                # because this usually forces a lot of recompilation. So we
                # create a marker file that indicates that we configured
                # containing the exact configure command as generated here to
                # remember how the build dir is currently configured.
                if builddir is None:
                    cfg_cmd = '../../%s/%s/configure\\\n' % (reponame, configuredir)
                else:
                    cfg_cmd = '%s/configure\\\n' % os.path.relpath(configuredir, builddir)
                cfg_cmd += '    %s\\\n' % configureformat.format(prefix=prefix, target=target)
                for flag in config:
                    if ' ' in flag:
                        cfg_cmd += '    "%s"\\\n' % flag
                    else:
                        cfg_cmd += '    %s\\\n' % flag
                
                cfg_cmd_fname = configdir + os.sep + 'build' + os.sep + 'cfg.cmd'
                reconfig = False
                if run_rvex_cfg:
                    status('going to configure because rvex-cfg.py was run')
                    reconfig = True
                elif not os.path.exists(cfg_cmd_fname):
                    status('going to configure because this is the first run')
                    reconfig = True
                else:
                    with open(cfg_cmd_fname, 'r') as ccf:
                        if cfg_cmd != ccf.read():
                            status('going to configure because configure flags changed')
                            reconfig = True
                
                if reconfig:
                    
                    # Create the new marker file, but use a different filename. We
                    # only force move it to the correct filename when configure
                    # reports success!
                    with open(cfg_cmd_fname + '.next', 'w') as ccf:
                        ccf.write(cfg_cmd)
                    f.write('echo "###build### %s: running configure"\n' % tool)
                    f.write('(' + cfg_cmd + '    && mv -f cfg.cmd.next cfg.cmd) || exit $?\n')
            
            # Make command.
            status('going to run make %s' % maketarget)
            f.write('echo "###build### %s: running make %s"\n' % (tool, maketarget))
            f.write('make %s\\\n' % maketarget)
            for flag in makeflags:
                if ' ' in flag:
                    f.write('    "%s"\\\n' % flag)
                else:
                    f.write('    %s\\\n' % flag)
            for key, value in makevars:
                f.write('    %s="%s"\\\n' % key, value)
            f.write('     || exit $?\n\n')
            
            # Install command.
            if installtarget:
                status('going to run make %s' % installtarget)
                f.write('echo "###build### %s: running make %s"\n' % (tool, installtarget))
                f.write('make %s\\\n' % installtarget)
                for key, value in makevars:
                    f.write('    %s="%s"\\\n' % key, value)
                f.write('     || exit $?\n\n')
        
        # Give execute permissions to the script.
        cmd('chmod +x "%s"', configdir + os.sep + 'build' + os.sep + 'build.sh')
        
        # BUILD SCRIPT EXECUTION
        # ======================
        
        # If we have a fixed build directory, prepare it.
        if builddir is not None:
            status('preparing fixed build directory')
            
            # Make sure that the build directory is clean, save for the files
            # in the ignore list.
            for fname in os.listdir(reponame + os.sep + builddir):
                if fname in builddirignore:
                    continue
                cmd('rm -rf "%s/%s/%s"', reponame, builddir, fname)
            
            # Move the contents of our build directory into the tool build
            # directory.
            for fname in os.listdir(configdir + os.sep + 'build'):
                cmd('mv "%s/build/%s" "%s/%s"', configdir, fname, reponame, builddir)
        
        # Now execute the build script in the appropriate way.
        try:
            if proot is None:
                status('running pending build commands')
                
                # Change to the right directory.
                prevwd = str(local.cwd)
                if builddir is None:
                    cd(configdir + os.sep + 'build')
                else:
                    cd(reponame + os.sep + builddir)
                
                # Simply execute the build script now.
                try:
                    cmd('./build.sh')
                finally:
                    cd(prevwd)
                
            else:
                status('running pending build commands in proot env')
                
                # Figure out the build directory relative to the rootfs root.
                if builddir is None:
                    workdir = '/%s/%s/build' % (tool, configdir)
                else:
                    workdir = '/%s/%s/%s' % (tool, reponame, builddir)
                
                # Execute the build script within the proot.
                cmd('../usr/bin/proot -r "%s" -b /dev -w "%s" %s ./build.sh',
                    os.path.abspath('..'), workdir, prootflags)
        
        # If we have a fixed build directory, move the build products into our
        # directory for future builds. We also want to do this if the build
        # fails.
        finally:
            if builddir is not None:
                status('saving fixed build directory state')
                for fname in os.listdir(reponame + os.sep + builddir):
                    if fname not in builddirignore:
                        cmd('mv "%s/%s/%s" "%s/build"', reponame, builddir, fname, configdir)
        
        # Write the "complete" file.
        with open(configdir + os.sep + 'complete', 'w') as f:
            f.write(str(datetime.datetime.now()) + '\n')
        
        # Copy output products. We do this file-by-file recursively and only
        # actually copy the file if the contents changed for build dependency
        # purposes.
        def copy_out(src_dir, dest_dir, root_dir=''):
            products = []
            
            # If the destination dir does not exist yet, create it.
            if not os.path.exists(dest_dir):
                cmd('mkdir -p "%s"', dest_dir)
            
            for fname in os.listdir(src_dir):
                src_fname = src_dir + os.sep + fname
                dest_fname = dest_dir + os.sep + fname
                if root_dir:
                    root_fname = root_dir + os.sep + fname
                else:
                    root_fname = fname
                
                # Always copy links.
                if os.path.islink(src_fname):
                    cmd('cp -fa "%s" "%s"', src_fname, dest_fname)
                    #products.append(root_fname) <- make doesn't handle symlinks in dependencies very well
                    continue
                
                # Recurse into directories.
                if os.path.isdir(src_fname):
                    products.extend(copy_out(src_fname, dest_fname, root_fname))
                    continue
                
                # Check if an identical output file already exists.
                copy = True
                if os.path.exists(dest_fname):
                    copy = False
                    with open(src_fname, "rb") as src_f:
                        with open(dest_fname, "rb") as dest_f:
                            src_data = True
                            while src_data:
                                src_data = src_f.read(4096)
                                dest_data = dest_f.read(4096)
                                if src_data != dest_data:
                                    copy = True
                                    break
                
                # The file changed or doesn't exist yet, so we need to copy.
                if copy:
                    if os.path.islink(dest_fname):
                        cmd('cp --remove-destination -a "%s" "%s"', src_fname, dest_fname)
                    else:
                        cmd('cp -fa "%s" "%s"', src_fname, dest_fname)
                
                # Record the file in the products list.
                products.append(root_fname)
            
            return products
        
        status('copying build products to ' + target)
        products = copy_out(configdir + os.sep + 'install', target)
        
        # Create a file that lists the products of this build. Like the actual
        # build products, we only modify this file if its contents need to
        # change.
        manifest = target + os.sep + 'manifest'
        if not os.path.exists(manifest):
            cmd('mkdir -p "%s"', manifest)
        
        manifest += os.sep + tool
        
        manifest_new = ''.join([s + '\n' for s in sorted(products)])
        
        manifest_cur = None
        if os.path.exists(manifest):
            with open(manifest, 'r') as f:
                manifest_cur = f.read()
        
        if manifest_new != manifest_cur:
            with open(manifest, 'w') as f:
                f.write(manifest_new)
            done_msg = 'done, updated manifest'
        else:
            done_msg = 'done, manifest unchanged'
        
    
    # CLEANUP
    # =======
    
    finally:
        try:
            
            # Restore overridden files.
            if os.path.exists('tmp-override'):
                status('restoring file overrides')
                
                # Restore overridden files.
                if os.path.exists('tmp-override/overridden'):
                    with open('tmp-override/overridden', 'r') as f:
                        overridden = filter(None, f.read().split('\n'))
                    for idx, dest in enumerate(overridden):
                        dir = reponame + os.sep + os.path.dirname(dest)
                        if not dir:
                            dir = '.'
                        if not os.path.isdir(dir):
                            cmd('-mkdir -p "%s"', dir)
                        cmd('-mv -f tmp-override/%d "%s/%s"', idx, reponame, dest)
                    cmd('-rm -f tmp-override/overridden')
                
                # Remove added files.
                if os.path.exists('tmp-override/added'):
                    with open('tmp-override/added', 'r') as f:
                        added = filter(None, f.read().split('\n'))
                    for f in added:
                        cmd('-rm -rf "%s/%s"', reponame, f)
                    cmd('-rm -f tmp-override/added')
                
                cmd('-rm -rf tmp-override')
            
            # Enter the git repository directory for cleanup operations.
            if os.path.exists('tmp'):
                status('restoring git repo state')
                cd(reponame)
                try:
                    
                    # Bring the repository in a well-defined state.
                    cmd('git reset --hard')
                    cmd('git clean -fdx')
                    
                    # Switch to the original branch.
                    if os.path.exists('../tmp/branch'):
                        with open('../tmp/branch', 'r') as f:
                            branch = f.read().strip()
                        if branch:
                            cmd('-git checkout %s', branch)
                        cmd('-rm -f ../tmp/branch')
                    
                    # Switch to the original revision if that is different now.
                    if os.path.exists('../tmp/revision'):
                        with open('../tmp/revision', 'r') as f:
                            old_revision = f.read().strip()
                        cur_revision = cmd('-git rev-parse HEAD', redir=True).strip()
                        if old_revision != cur_revision:
                            cmd('-git checkout %s', old_revision)
                        cmd('-rm -f ../tmp/revision')
                    
                    # Restore modified files.
                    if os.path.exists('../tmp/modified'):
                        with open('../tmp/modified', 'r') as f:
                            modified = filter(None, f.read().split('\n'))
                        for idx, dest in enumerate(modified):
                            dir = os.path.dirname(dest)
                            if not dir:
                                dir = '.'
                            if not os.path.isdir(dir):
                                cmd('-mkdir -p "%s"', dir)
                            cmd('-mv -f ../tmp/%d "%s"', idx, dest)
                        cmd('-rm -f ../tmp/modified')
                    
                    # Remove previously deleted files.
                    if os.path.exists('../tmp/deleted'):
                        with open('../tmp/deleted', 'r') as f:
                            deleted = filter(None, f.read().split('\n'))
                        for f in deleted:
                            cmd('-rm -f "%s"', f)
                        cmd('-rm -f ../tmp/deleted')
                    
                    cmd('-rm -rf ../tmp')
                    
                finally:
                    cd('..')
        
        finally:
            
            # Release the lock to the rootfs/tool directory.
            cmd('rm -f %s', lockf)
            cd('../..')
            
            if done_msg is not None:
                status(done_msg)


if __name__ == '__main__':
    
    # PARSE COMMAND LINE
    # ==================
    import argparse
    
    parser = argparse.ArgumentParser(
        usage = '%(prog)s [options] [rootfs] <tool> <target>',
        description = 'r-VEX tool build system. This tool wraps the build systems of the individual\n'
                      'tools to add:\n'
                      ' - automatic downloads and GIT repository management\n'
                      ' - automatic configuration difference detection\n'
                      ' - intermediate file storage for multiple builds\n'
                      ' - barriers using file locks to allow multithreading\n'
                      ' - file overrides to change configuration files prior to building\n'
                      ' - building in a proot\'ed environment\n'
                      'and probably some other things that I forgot to mention.',
        formatter_class = argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument('rootfs',
        help = 'Specifies the rootfs to use. Tries to figure this out based on <tool> by default.',
        default = None,
        nargs = '?',
        action = 'store')
    
    parser.add_argument('tool',
        help = 'Specifies the tool to build.',
        action = 'store')
    
    parser.add_argument('target',
        help = 'Target directory for install products.',
        action = 'store')
    
    parser.add_argument('-t', '--toolcfg',
        help = 'Tool configuration json file to run rvex-cfg.py with.',
        action = 'store')
    
    parser.add_argument('-c', '--clean',
        help = 'Cleans prior to building.',
        action = 'store_true')
    
    parser.add_argument('--clean-only',
        help = 'Cleans the directory that would be used for the given configuration, '
               'but doesn\'t build it.',
        action = 'store_true')
    
    parser.add_argument('-p', '--pull',
        help = 'Pulls the GIT repository prior to building. The master branch is '
               'pulled unless -r/--revision is specified. Even when this is specified, '
               'the repository is reverted to the previous state after the build. '
               'You will need to run git pull etc. manually if you want to keep the '
               'new state.',
        action = 'store_true')
    
    parser.add_argument('-r', '--revision',
        help = 'The GIT revision, tag, or branch to check out prior to building. If not '
               'specified, and -p/--pull is also not specified, the current working '
               'tree is used.',
        metavar = '<sha1/tag/branch>',
        action = 'store',
        default = None)
    
    parser.add_argument('-i', '--initrevision',
        help = 'If the GIT repository is first cloned by this command, check out this '
               'revision after cloning.',
        metavar = '<sha1/tag/branch>',
        action = 'store',
        default = None)
    
    parser.add_argument('-C', '--configure',
        help = 'Adds a command line parameter for `configure` (use "-C=--flag" for flags).',
        metavar = '<arg>',
        action = 'append')
    
    parser.add_argument('-E', '--environment',
        help = 'Adds/overrides an environment variable for the build process.',
        metavar = '<name>=<value>',
        action = 'append')
    
    parser.add_argument('-M', '--makevar',
        help = 'Overrides a make variable.',
        metavar = '<name>=<value>',
        action = 'append')
    
    parser.add_argument('-F', '--make',
        help = 'Adds a command line parameter for `make` (use "-F=--flag" for flags).',
        metavar = '<arg>',
        action = 'append')
    
    parser.add_argument('-j',
        help = 'Tells make to build in parallel. Unlike make, the thread count is mandatory.',
        metavar = '<threads>',
        action = 'store',
        default = 1,
        type = int)
    
    parser.add_argument('-O', '--override',
        help = 'Overrides a file in the source tree prior to building. <dest> is the file in '
               'the source tree relative to the repository root, <src> is the file that it '
               'should be overwritten with relative to the current working directory.',
        metavar = '<dest>=<src>',
        action = 'append')
    
    parser.add_argument('-R', '--remove',
        help = 'Removes a file in the source tree prior to building. <file> is the file to '
               'be removed relative to the repository root.',
        metavar = '<file>',
        action = 'append')
    
    parser.add_argument('-n', '--name',
        help = 'Name for this build. Defaults to "default". This can be used '
               'to have multiple configuration directories (each with their own '
               'intermediate files etc.) sharing the same configure/make variable '
               'configuration.',
        metavar = '<name>',
        action = 'store',
        default = 'default')
    
    args = parser.parse_args()
    
    try:
        
        # POST-PROCESS COMMAND LINE OPTIONS
        # =================================
        
        # If rootfs is not specified, figure it out for ourselves.
        if args.rootfs is None:
            try:
                config_json = readconfig()
            except (IOError, ValueError) as e:
                BuildError.fmt('Could not read tools.json: %s', e)
            for rootfs, config in config_json.items():
                if args.tool in config['tools']:
                    break
            else:
                BuildError.fmt('Could not find the specified tool in tools.json')
        else:
            rootfs = args.rootfs
        
        def vlist(lst):
            return [] if lst is None else lst
        
        def kvlist(kvs_in):
            kvs = []
            for kv in vlist(kvs_in):
                pair = kv.split('=', 1)
                if len(pair) != 2:
                    BuildError.fmt('Missing = sign in key=value argument')
                kvs.append(tuple(map(str.strip, pair)))
            return kvs
        
        # Postprocess the other parameters.
        tool = args.tool
        target = args.target
        toolcfg = args.toolcfg
        config = vlist(args.configure)
        env = kvlist(args.environment)
        makevars = kvlist(args.makevar)
        makeflags = vlist(args.make)
        makeflags.append('-j%d' % args.j)
        files = kvlist(args.override)
        files += [(x, None) for x in vlist(args.remove)]
        revision = args.revision
        init_revision = args.initrevision
        configdir = args.name
        clean = args.clean or args.clean_only
        pull = args.pull
        clean_only = args.clean_only
        
        # PERFORM THE BUILD
        # =================
        build(rootfs, tool, target, toolcfg, config, env, makevars, makeflags, files, revision, init_revision, configdir, clean, pull, clean_only)
    
    except DontBuild as e:
        print('###build### %s: %s' % (args.tool, e), file=sys.stderr)
        sys.stderr.flush()
        sys.exit(0)
    
    except BuildError as e:
        print('###build### %s: %s' % (args.tool, e), file=sys.stderr)
        sys.stderr.flush()
        sys.exit(1)
    
