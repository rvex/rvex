r-VEX main repository
=====================

This repository works as a central hub for all the other repositories. It
consists of the following things:

 - tools: the toolchain builder.
 - legacy: toolchain build script for the legacy release structure (the one
   without proper tool builder that just uses a few precompiled toolchains).
 - platforms: location for platform subrepositories. Refer to the readme file
   in that directory for further instructions.

The toolchain builder will automatically clone the necessary subrepositories the
first time you run a build. If, at that time, a `release-tag` file exists in the
root of this repository, the tag/branch specified in that file will be checked
out for all these subrepositories.

Dependencies
============

The r-VEX system needs a 64-bit Linux-like environment supporting symlinks.
Because of the latter, emulators for Windows such as Cygwin will probably be a
pain to get working. OpenSUSE is recommended because it was used to test the
release, but you should be able to get everything working on any major
distribution. Specifically, the package lists below are for OpenSUSE Leap 42.3,
as it was on the 9th of February 2018.

HDL software packages
---------------------

 - Xilinx ISE 14.7 (for Virtex 6 platforms)
 - Xilinx Vivado (for 7-series platforms)
 - Modelsim

You have to install these manually, and you need to have licenses to use them.
If you use different FPGAs or a different simulator, you will need to modify
the scripts or build your own platform. The latter is recommended, in which
case you can use the toolchain builder from https://bitbucket.org/rvex/rvex.
In that case, you need only the repository packages listed in the following
sections.

Repository packages
-------------------

On OpenSUSE, you can install these using `sudo zypper install <name>`. If you
use a different distribution, you may need to use `apt-get`, `yum`, `apper`
etc. instead. The package names may also be slightly different, unfortunately.

 - make           - pretty much everything uses make for building.
 - patch          - used here and there.
 - git            - used to download and manage the tool repositories.
 - gcc            - C compiler.
 - gcc-c++        - C++ compiler.
 - makeinfo       - used by binutils.
 - bison          - used by binutils.
 - m4             - used by binutils.
 - flex           - used by binutils.
 - binutils-devel - used by simrvex.
 - libX11         - used by simrvex.
 - libX11-devel   - used by simrvex.
 - libXpm         - used by simrvex.
 - libXpm-devel   - used by simrvex.

The rvex-hdl repository can generate the user manual for the r-VEX based on
some configuration stuff. This is done using `pdflatex` and a couple of
additional scripts; you need the following packages for this to work. The
buildsystem ignores errors from this target, so if you're not interested in the
generated documentation you *don't* need these.

 - texlive-latex  - LaTeX engine.
 - perl           - used to filter pdflatex's stderr output.
 - texlive-tabto-ltx
 - texlive-import
 - texlive-todonotes

Some tools that get shipped with OpenSUSE may not get shipped with your OS.
Here's an (incomplete) list of such tools that you need.

 - bash           - many .sh scripts depend on features not present in sh.
 - python3        - pretty much everything uses python3 for scripting.
 - wget           - used to fetch some additional data from the TU Delft FTP.
 - curl           - same usage as above... inconsistencies, sorry.
 - netcat         - used to communicate with the r-VEX debug interface server.

Finally, some python3 packages should be installed. You can usually install
these using `sudo pip3 install <package>`.

 - plumbum        - used by some complicated scripts in place of shell
                    scripting.

